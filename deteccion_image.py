from PIL import Image, ImageDraw, ImageFont
from azure.cognitiveservices.vision.contentmoderator import ContentModeratorClient
import azure.cognitiveservices.vision.contentmoderator.models
from azure.cognitiveservices.vision.face import FaceClient
from msrest.authentication import CognitiveServicesCredentials
import azure.cognitiveservices.speech as speechsdk
import time
import os

# Funciones

'''
- Segun el porcentaje de certeza de cada emocion se devuelve el String con la emocion con mas certeza
@Param azure.cognitiveservices.vision.face.models._models_py3.Emotion
@Return String
'''


def detect_Emotion(emotion):
    # Porcentaje de certeza al que debe superar
    percent_certainty = 95
    # Convertimos emociones en porcentajes
    neutral = emotion.neutral * 100
    happiness = emotion.happiness * 100
    anger = emotion.anger * 100
    sadness = emotion.sadness * 100

    contempt = emotion.contempt * 100
    disgust = emotion.disgust * 100
    fear = emotion.fear * 100
    surprise = emotion.surprise * 100
    if neutral > percent_certainty:
        return "Neutral"
    elif happiness > percent_certainty:
        return "Feliz"
    elif anger > percent_certainty:
        return "Enojado"
    elif sadness > percent_certainty:
        return "Triste"
    elif contempt > percent_certainty:
        return "Desprecio"
    elif disgust > percent_certainty:
        return "Disgustado"
    elif fear > percent_certainty:
        return "Temeroso"
    elif surprise > percent_certainty:
        return "Sorprendido"
    return "Desconocido"


'''
- Segun el resultado del genero se devuelve un String del genero de la cara detectada y en español
@Param Enum 'Gender'
@Return String
'''


def detect_Gender(gender):
    if gender.value == 'male':
        return 'Hombre'
    elif gender.value == 'female':
        return 'Mujer'
    return gender.value


# Variables para el API AZURE
# This key will serve all examples in this document.
API_KEY = "488b275b0ed64cf8b34e79fcf6069b3a"
# This endpoint will be used in all examples in this quickstart.
ENDPOINT = "https://faceapiazureia.cognitiveservices.azure.com"

'''
- Recorre las imagenes del video para aplicarle su debida deteccion, ya sea,
- Edad, Emocion, Genero
- Además, guarda los archivos generados en la carpeta Output
@Param String url_video
@Return void
'''


def deteccion_imageVideo(imageFile):
    try:
        face_client = FaceClient(
            ENDPOINT, CognitiveServicesCredentials(API_KEY))

        # Imagen que vamos a analizar
        img_file = open(imageFile, 'rb')
        # Se realiza analisis con el cliente de azure de deteccion de caras.
        response_detection = face_client.face.detect_with_stream(
            image=img_file,
            detection_model='detection_01',
            recognition_model='recognition_04',
            return_face_attributes=['age', 'emotion', 'gender', 'hair']
        )
        # Abrimos la imagen original para clonarla
        img = Image.open(img_file)
        # Usado para dibujar en la imagen
        draw = ImageDraw.Draw(img)
        # Tipo de letra que mostramos resultados en la imagen
        font = ImageFont.truetype('C:\Windows\Fonts\ARLRDBD.ttf', 20)

        # Recorremos cada cara detectada
        for face in response_detection:
            age = face.face_attributes.age  # Obtenemos la edad
            # Obtenemos las emociones y la convertimos en una unica emocion String
            emotion = detect_Emotion(face.face_attributes.emotion)
            gender = detect_Gender(face.face_attributes.gender)

            #print("Cara "+ str(face.face_id) +": "+ emotion +" " + str(face.face_attributes.emotion))

            # Obtenemos el rectangulo para pintarlo sobre la cara detectada
            rect = face.face_rectangle
            # Obtenemos la posicion de cada lado del rectangulo que vamos a pintar en la cara detectada
            left = rect.left
            top = rect.top
            right = rect.width + left
            botton = rect.height + top
            # Dibujamos el rectangulo verde sobre el cara detectado
            draw.rectangle(((left, top), (right, botton)),
                           outline='green', width=5)

            # Dibujamos letras mostrando la edad de la cara detectada
            draw.text((left, botton), 'Edad: '+str(int(age)), fill=(255,
                                                                    255, 255), font=font, stroke_width=1, stroke_fill="black")

            # Dibujamos letras mostrando la emocion de la cara detectada
            # Stroke = linea alrededor de letras.
            draw.text((left-45, botton+20), 'Emocion: '+emotion, fill=(255,
                                                                       255, 255), font=font, stroke_width=1, stroke_fill="black",)
            # Dibujamos letras mostrando el genero de la cara detectada
            draw.text((left-45, botton+40), 'Genero: '+gender, fill=(255,
                                                                     255, 255), font=font, stroke_width=1, stroke_fill="black")

        if(len(response_detection) == 0):
            return 0
        else:
            # Guardamos la nueva imagen detectada
            img.save("./ResultadosVision/"+imageFile)
            return ([age, emotion, gender])

        # Muestra la nueva imagen modificada con las caras detectadas
        # print('Cantidad de personas detectadas: {0}'.format(
        #    len(response_detection)))
    except Exception as err:
        print("No se pudo hacer la deteccion de: "+imageFile)
        return 0


def deteccion_image(imageFile):
    try:
        start = time.perf_counter()
        os.mkdir(str('ResultadosVision'))  # crea una nueva carpeta
        face_client = FaceClient(
            ENDPOINT, CognitiveServicesCredentials(API_KEY))

        # Imagen que vamos a analizar
        img_file = open(imageFile, 'rb')
        # Se realiza analisis con el cliente de azure de deteccion de caras.
        response_detection = face_client.face.detect_with_stream(
            image=img_file,
            detection_model='detection_01',
            recognition_model='recognition_04',
            return_face_attributes=['age', 'emotion', 'gender', 'hair']
        )
        # Abrimos la imagen original para clonarla
        img = Image.open(img_file)
        # Usado para dibujar en la imagen
        draw = ImageDraw.Draw(img)
        # Tipo de letra que mostramos resultados en la imagen
        font = ImageFont.truetype('C:\Windows\Fonts\ARLRDBD.ttf', 20)

        # Recorremos cada cara detectada
        for face in response_detection:
            age = face.face_attributes.age  # Obtenemos la edad
            # Obtenemos las emociones y la convertimos en una unica emocion String
            emotion = detect_Emotion(face.face_attributes.emotion)
            gender = detect_Gender(face.face_attributes.gender)

            #print("Cara "+ str(face.face_id) +": "+ emotion +" " + str(face.face_attributes.emotion))

            # Obtenemos el rectangulo para pintarlo sobre la cara detectada
            rect = face.face_rectangle
            # Obtenemos la posicion de cada lado del rectangulo que vamos a pintar en la cara detectada
            left = rect.left
            top = rect.top
            right = rect.width + left
            botton = rect.height + top
            # Dibujamos el rectangulo verde sobre el cara detectado
            draw.rectangle(((left, top), (right, botton)),
                           outline='green', width=5)

            # Dibujamos letras mostrando la edad de la cara detectada
            draw.text((left, botton), 'Edad: '+str(int(age)), fill=(255,
                                                                    255, 255), font=font, stroke_width=1, stroke_fill="black")

            # Dibujamos letras mostrando la emocion de la cara detectada
            # Stroke = linea alrededor de letras.
            draw.text((left-45, botton+20), 'Emocion: '+emotion, fill=(255,
                                                                       255, 255), font=font, stroke_width=1, stroke_fill="black",)
            # Dibujamos letras mostrando el genero de la cara detectada
            draw.text((left-45, botton+40), 'Genero: '+gender, fill=(255,
                                                                     255, 255), font=font, stroke_width=1, stroke_fill="black")

            detect_contentmoderator(imageFile)
        if(len(response_detection) == 0):
            return 0
        else:
            # Guardamos la nueva imagen detectada
            img.save("./ResultadosVision/"+imageFile)
            print("La ejecución del archivo: "+imageFile +
                  " finalizó en un tiempo de:" + str(time.perf_counter() - start))

        # Muestra la nueva imagen modificada con las caras detectadas
        # print('Cantidad de personas detectadas: {0}'.format(
        #    len(response_detection)))
    except Exception as err:
        print("No se pudo hacer la deteccion de: "+imageFile)
        return 0


def detect_contentmoderator(imageFile):
    CONTENT_MODERATOR_ENDPOINT = "https://contentmoderatoroperativos.cognitiveservices.azure.com/"
    subscription_key = "26f3f6ba3dcc4b1e8ff1d5d1f1b3bffd"

    client = ContentModeratorClient(
        endpoint=CONTENT_MODERATOR_ENDPOINT,
        credentials=CognitiveServicesCredentials(subscription_key)
    )

    img_file = open(imageFile, 'rb')

    # The following code checks the image at the given URL for adult or racy content and prints results to the console.
    evaluation = client.image_moderation.evaluate_file_input(
        content_type="application/json",
        cache_image=True,
        image_stream=img_file
    )
    print("Resultado contenido")
    print("Nombre del archivo: "+imageFile)
    print("Contenido para adultos: "+str(evaluation.result))
    print("Contenido picante: "+str(evaluation.adult_classification_score))
    print("Contenido picante:"+ str(evaluation.is_image_adult_classified))
    print("Calificaion:"+ str(evaluation.is_image_racy_classified))