# Detección de Objetos con Azure Cognitive y Yolov3
La finalidad de este proyecto era la utilización de los servicios cognitivos de Azure en conjunto con un extractor de imágenes con un objeto en específico, en este caso personas, para la detección de información como los serían la edad, género, emoción y revision de contenido moderado en caso de que esta tenga.

## Guía de instalación y ejecución del modelo de detección de objetos

### Requerimientos previos
Para esta guía de instalación se requiere tener previamente instalado python 3.6 o superior, Anaconda3 y utilizar la consola de Anaconda (o la del sistema en caso de haber configurado la dirección de Anaconda en el PATH).

### Crear ambiente en anaconda
Para tener en orden nuestras paqueterías de python primero es necesario un ambiente llamado "deteccionAzure" con la versión 3.6 de python.
``` 
conda create -n deteccionAzure python=3.6
``` 
Activamos el ambiente "deteccionAzure" para asegurarnos que estemos en el ambiente correcto al momento de hacer la instalación de todas las paqueterías necesarias.
``` 
activate deteccionAzure
``` 
### Instalación componentes
Antes de instalar los requerimientos en el ambiente es necesario intalar pytorch, torchvision y cudatoolkit en el mismo ambiente de trabajo.
``` 
conda install pytorch==1.5.0 torchvision==0.6.0 cudatoolkit=9.2 -c pytorch
``` 
Además, se requiere instalar las librerías de Azure a utilizar.
``` 
pip install --upgrade azure-cognitiveservices-vision-face
pip install --upgrade azure-cognitiveservices-vision-contentmoderator
pip install --upgrade azure-cognitiveservices-speech

``` 
### Instalación de las paqueterías
Una vez dentro del ambiente y con los componentes extra ya instalados, vamos a instalar todas las paqueterías necesarias para poder ejecutar el proyecto.
``` 
pip install -r requirements.txt
``` 

### Ejecución del detector de objetos
Para detectar en un video o imagen las emociones, edad, genero, verificar si tiene contenido +18 e incluso guardar el audio del video.
Se debe ejecutar el siguiente comando en este respectivo formato:

```
python main.py [nombreArchivo]* [Opcion]
```
Pueden ser uno o multiples nombreArchivo pero una única opción.
Algunas de sus entradas son:
```
nombreArchivo:String = "Video.mp4 escena.avi" or "foto.png" or "foto.jpg" or entre otros
Opcion:Int = 0 or 1 
    0: Video
    1: Imagen
```

Ejemplo:

```
# Comando para un unico video
python main.py video.mp4 0

# Comando para multiples videos
python main.py video.mp4 video1.avi video2.mp4 0

# Comando para una unica imagen
python main.py foto.png 1

# Comando para multiples imagenes
python main.py foto.png foto1.jpg video2.jpeg 1

```


## Resultados Obtenidos


**Imagen**: Se crea una carpeta en 'outputs' llamada ResultadosVision que contiene:
- Fotos editadas de cada imagen enviada con sus debidas deteciones faciales.
- Archivo txt con resultados acerca del analisis para verificar contenido +18

**Video**: Para cada video se creará una carpeta en 'outputs' con el nombre del video que contiene:
- Fotos de personas en el video
- Carpeta llamada ResultadosVision
    - Fotos con la detecion del rostro, ademas de su edad, emocion y genero
    - Archivo txt con resultados acerca del analisis para verificar contenido 18+
- Carpeta llamada ResultadosSpeech
    - Audio del video enviado.
    - Archivo txt con transcripcion del video.

