# Multiprocesamiento Main
from deteccion_video import deteccion_video
from deteccion_image import deteccion_image
from deteccion_image import detect_contentmoderator
import sys
import time
import os
import concurrent.futures


if __name__ == "__main__":
    listaNombreArchivos = []
    for i in sys.argv:
        listaNombreArchivos.append(str(i))
    listaNombreArchivos.pop(0)
    option = int(listaNombreArchivos.pop(-1))
    '''
    El método ThreadPoolExecutor trabaja de manera similar a ProcessPoolExecutor, pero se trata de mecanismos distintos:
    ProcessPoolExecutor ejecuta cada worker como un proceso hijo separado   
    ThreadPoolExecutor ejecuta cada uno de los workers como un subproceso (thread) dentro del proceso principal
    '''
    # El método "ProcessPoolExecutor" permite
    # ejecutar varias instancias (workers)

    print("-----------------Datos de ejecucion-----------------" + '\n')
    start = time.perf_counter()

    with concurrent.futures.ProcessPoolExecutor() as executor:
        for i in range(0, len(listaNombreArchivos)):
            if option == 0:
                executor.submit(deteccion_video, listaNombreArchivos[i])
            elif option == 1:
                executor.submit(deteccion_image, listaNombreArchivos[i])
            else:
                print('Opción Inválida')
    print("----------------------------------------------------")
