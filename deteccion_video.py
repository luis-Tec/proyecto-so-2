from __future__ import division
from models import *
from utils.utils import *
from utils.datasets import *
from PIL import Image
from torch.autograd import Variable
from pathlib import Path
from deteccion_image import deteccion_imageVideo, detect_contentmoderator
import azure.cognitiveservices.speech as speechsdk
import os
import sys
import argparse
import cv2
import torch
import time

# Libreria para extraer el audio de un video
import moviepy.editor

# Libreria para graficar
import matplotlib
import matplotlib.pyplot as plt
import numpy as np


def Convertir_RGB(img):
    # Convertir Blue, green, red a Red, green, blue
    b = img[:, :, 0].copy()
    g = img[:, :, 1].copy()
    r = img[:, :, 2].copy()
    img[:, :, 0] = r
    img[:, :, 1] = g
    img[:, :, 2] = b
    return img


def Convertir_BGR(img):
    # Convertir red, blue, green a Blue, green, red
    r = img[:, :, 0].copy()
    g = img[:, :, 1].copy()
    b = img[:, :, 2].copy()
    img[:, :, 0] = b
    img[:, :, 1] = g
    img[:, :, 2] = r
    return img


def Generar_grafico(diccionario, nombre):
    # Generar un grafico
    fig, ax = plt.subplots()
    # Etiqueta en el eje Y
    ax.set_ylabel('Datos')
    # Etiqueta en el eje X
    ax.set_title('Cantidad de detecciones por '+nombre)
    for x in diccionario:
        plt.bar(x, diccionario[x])
    plt.savefig('./ResultadosVision/Grafico'+nombre+'.png')


def deteccion_video(url_video):
    # Variables locales
    localDir = ""
    start = time.perf_counter()
    fileName = str(url_video)
    auxFrame = 0
    auxOutputName = 0
    diccionarioEmociones = {}
    diccionarioGenero = {}
    start = time.perf_counter()

    # Definicion de parametros a utilizar
    image_folder = "data/samples"
    model_def = "config/yolov3.cfg"
    weights_path = "weights/yolov3.weights"
    class_path = "data/coco.names"
    conf_thres = 0.8
    nms_thres = 0.4
    batch_size = 2
    n_cpu = 0
    img_size = 416
    directorio_video = fileName
    checkpoint_model = ""
    video = moviepy.editor.VideoFileClip(url_video)

    # Uso del cpu o gpu según esté disponible
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    model = Darknet(model_def, img_size=img_size).to(device)

    # Pesos del modelo a cargar
    if weights_path.endswith(".weights"):
        model.load_darknet_weights(weights_path)
    else:
        model.load_state_dict(torch.load(weights_path))
    model.eval()
    classes = load_classes(class_path)
    Tensor = torch.cuda.FloatTensor if torch.cuda.is_available() else torch.FloatTensor

    # Obtiene el video a analizar.
    cap = cv2.VideoCapture(directorio_video)
    colors = np.random.randint(
        0, 255, size=(len(classes), 3), dtype="uint8")

    # Carpeta donde se guardan los resultados
    os.chdir('outputs')  # accede al directorio
    os.mkdir(str(fileName))  # crea una nueva carpeta
    os.chdir(str(fileName))  # accede al directorio
    os.mkdir(str('ResultadosVision'))  # crea una nueva carpeta
    os.mkdir(str('ResultadosSpeech'))  # crea una nueva carpeta
    localDir = str(os.getcwd())

    audio = video.audio
    audio.write_audiofile("./ResultadosSpeech/audio.wav")
    transcripcion_video()

    while cap:
        ret, frame = cap.read()
        # Mostrar Segundos del frame (cap.get(cv2.CAP_PROP_POS_MSEC)/1000)
        if ret is False:
            break
        frame = cv2.resize(frame, (1280, 960),
                           interpolation=cv2.INTER_CUBIC)
        # LA imagen viene en Blue, Green, Red y la convertimos a RGB que es la entrada que requiere el modelo
        RGBimg = Convertir_RGB(frame)
        imgTensor = transforms.ToTensor()(RGBimg)
        imgTensor, _ = pad_to_square(imgTensor, 0)
        imgTensor = resize(imgTensor, 416)
        imgTensor = imgTensor.unsqueeze(0)
        imgTensor = Variable(imgTensor.type(Tensor))

        with torch.no_grad():
            detections = model(imgTensor)
            detections = non_max_suppression(
                detections, conf_thres, nms_thres)

        for detection in detections:
            # Cada 20 frames guarda una imagen
            if auxFrame == 30:
                auxFrame = 0

            if detection is not None:
                detection = rescale_boxes(
                    detection, img_size, RGBimg.shape[:2])
                for x1, y1, x2, y2, conf, cls_conf, cls_pred in detection:
                    box_w = x2 - x1
                    box_h = y2 - y1
                    color = [int(c) for c in colors[int(cls_pred)]]
                if float(conf) >= 0.90 and auxFrame == 0 and str(classes[int(cls_pred)]) == "person":
                    # Escribe los datos
                    cv2.imwrite(
                        str(auxOutputName)+'.jpg', Convertir_RGB(frame))
                    auxOutputName += 1

                auxFrame += 1

    #
    result = [f for f in os.listdir(str(os.getcwd())) if os.path.isfile(
        os.path.join(str(os.getcwd()), f))]
    for i in range(0, len(result)):
        aux = deteccion_imageVideo(result[i])
        if(aux != 0):

            # Agrega nuevas emociones o las incrementa en el diccionario
            if aux[1] in diccionarioEmociones:
                diccionarioEmociones[aux[1]] += 1
            else:
                diccionarioEmociones[aux[1]] = 1

                # Incrementa el genero encontrado en el diccionario
            if aux[2] in diccionarioGenero:
                diccionarioGenero[aux[2]] += 1
            else:
                diccionarioGenero[aux[2]] = 1

    # Genera un grafico con los datos recolectados
    Generar_grafico(diccionarioEmociones, 'Emociones')
    Generar_grafico(diccionarioGenero, 'Genero')

 # Archivo con la informacion
    file = open('./ResultadosVision/ResultadosContenido+18.txt', "w")
    file.write("------------Resultados de la detección------------" + '\n')

    result = [f for f in os.listdir(str(os.getcwd())) if os.path.isfile(
        os.path.join(str(os.getcwd()), f))]
    for i in range(0, len(result)):
        detect_contentmoderator(result[i])
        
    print("La ejecución del archivo: "+fileName +
          " finalizó en un tiempo de:" + str(time.perf_counter() - start))

    # Se cierran todos los procesos
    cap.release()
    cv2.destroyAllWindows()
    file.close()


# Funcion encargada de transcribir a un txt el audio del video
def transcripcion_video():
    speech_config = speechsdk.SpeechConfig(
        subscription="84bf95013d154c62bfadda716e13a04c", region="westus2")
    audio_input = speechsdk.AudioConfig(
        filename="./ResultadosSpeech/audio.wav")
    speech_recognizer = speechsdk.SpeechRecognizer(
        speech_config=speech_config, audio_config=audio_input)
    result = speech_recognizer.recognize_once_async().get()
    file = open('./ResultadosSpeech/TranscripcionVideo.txt', "w")
    file.write(result.text)
